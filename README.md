# SFMoives
## Intro
This is a REST API built using Jersey. The JSON is parsed using gson.
<br>URL: https://maxglaidasfmovies.herokuapp.com/

## Usage
The basic web view has three options.<br>
the first one is to filter the results by Title. To get a valid result enter for example:Ant-Man
the call to this API endpoint is:
```url
https://maxglaidasfmovies.herokuapp.com/Services/SFMovies/filterTitle?title=Ant-Man
```
The 2nd Option is to filter by location. To get a valid answer enter: T - this will give all locations starting with a T.
```
https://maxglaidasfmovies.herokuapp.com/Services/SFMovies/filterLocation?location=T
```
The 3rd option is a link that gives back all the data set back without filtering.
```
https://maxglaidasfmovies.herokuapp.com/Services/SFMovies/
```

# Filtering by Title
The solution here was to get the data with a simple api call, transform it into Movie objects using gson and then filter.
The filtered list is then transformed back into a JSON format and sent to the User.

# Filtering by Location
Here i use a query to the San francisco API that gives me back already filtered results.
These results are than sent to the end user.