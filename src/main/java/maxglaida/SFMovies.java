package maxglaida;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import maxglaida.models.Movie;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

@Path("SFMovies")
public class SFMovies {


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAll() {
        String s = getStringResponse("https://data.sfgov.org/resource/wwmu-gmzc.json?$select=title,locations");
        return s;
    }

    @GET
    @Path("/filterTitle")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFilteredTitle(@QueryParam("title") String title) {
        if(title == null) return getAll();
        String raw = getAll();

        Gson gson = new Gson();
        Type listType = new TypeToken<List<Movie>>(){}.getType();
        System.out.println("Starting to filter list...");
        List<Movie> movieList = gson.fromJson(raw, listType);
        List<Movie> temp = new ArrayList<>();
        movieList.stream()
                .filter(Objects::nonNull) // get rid of null banks
                .filter(movie -> movie.getTitle() != null) // get rid of null accounts
                .filter(movie -> movie.getTitle().startsWith(title)) // get rid of empty accounts
                .forEach(temp::add);

        String filtered = gson.toJson(temp);
        System.out.println("Filtered Json: \n" + filtered );
        return filtered;
    }

    @GET
    @Path("/filterLocation")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFilteredLocation(@QueryParam("location") String location) {
        if(location == null) return getAll();
        location = location.replaceAll("\\s","%20");
        String temp = "";
        temp = location;
        temp.replace("P","M");
        String raw = getStringResponse(
                "https://data.sfgov.org/resource/wwmu-gmzc.json?$select=locations,title&$where=starts_with(locations,'"+location+"')"
        );
        return raw;
    }

    private String getStringResponse(String query) {
        try {
            URL url = new URL(query);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            int responsecode = conn.getResponseCode();
            if (responsecode != 200)
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            System.out.println("\nParsing response..");
            StringBuilder inline = new StringBuilder();
            Scanner scanner = new Scanner(url.openStream());
            while(scanner.hasNext()) {
                inline.append(scanner.nextLine());
            }
            System.out.println("Finished..");
            System.out.println(inline.toString());
            scanner.close();
            return inline.toString();
        } catch (IOException e){e.printStackTrace(); }

        return "The request could not be completed...";
    }

}
