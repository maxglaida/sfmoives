package maxglaida.models;

import com.google.gson.annotations.SerializedName;

public class Movie {

    @SerializedName("locations")
    private String locations;
    @SerializedName("title")
    private String title;

    public Movie() {
    }

    public Movie(String title, String locations) {
        setTitle(title);
        setLocations(locations);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        if(locations == null) {
            this.locations = "location unknown";
        }
        this.locations = "blabla";
    }
}
