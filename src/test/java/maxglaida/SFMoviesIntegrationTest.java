package maxglaida;

import static io.restassured.RestAssured.expect;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.notNullValue;

import io.restassured.http.ContentType;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(Integration.class)
public class SFMoviesIntegrationTest {

    @Test
    public void testOK() {
        expect().
                statusCode(200).
                when().
                get("/Services/SFMovies");
    }

    @Test
    public void testJSONContent() {
        expect().
                statusCode(200).
                contentType(ContentType.JSON).
                when().
                get("/Services/SFMovies");
        expect().
                statusCode(200).
                contentType(ContentType.JSON).
                when().
                get("/Services/SFMovies/filterLocation");
        expect().
                statusCode(200).
                contentType(ContentType.JSON).
                when().
                get("/Services/SFMovies/filterTitle?title='180'");
    }

    @Test
    public void testStringWithSpacesLocationFilter() {
        String json = "[{\"locations\":\"Pine between Kearney and Davis\",\"title\":\"Ant-Man\"}]";
        given().
                log().
                all().
                contentType(ContentType.JSON).
                get("/Services/SFMovies/filterLocation?location=Pine between Kearney and Davis").
                then().
                log().
                all().
                assertThat().body(containsString(json));

    }

    @Test
    public void testLocationCorrectParameter() {
        given().
                contentType(ContentType.JSON).
                get("/Services/SFMovies/filterTitle?title='180'").
                then().
                assertThat().statusCode(200).body(notNullValue());
    }







}